import java.lang.Math;
 
public class BinarySearch {

	public int search(int[] nums, int target) {
		if (nums != null && nums.length > 0) {
			return search(nums, target, 0, nums.length - 1);
		}
		return -1;
	}

	public int search(int[] nums, int target, int start, int end) {
		if (start <= end) {
			int center = (int) Math.ceil((start + end) / 2);
			if (nums[center] < target) {
				return search(nums, target, center + 1, end);
			} else if (nums[center] > target) {
				return search(nums, target, start, center - 1);
			} else {
				return center;
			}
		}
		return -1;
	}
}